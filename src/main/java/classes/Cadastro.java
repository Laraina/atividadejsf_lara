
package classes;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class Cadastro {
    
    @PersistenceContext(unitName="com.mycompany_BasicoJsf_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public Cadastro() {
    }
    
    public void salvar(Pessoa p){
        
        Pessoa existe = (Pessoa) em.find(Pessoa.class, p.getCpf());
        if (existe==null){
            em.persist(p);
        }
        
    }
        
    public void deletar(Pessoa p){
        em.remove(em.merge(p));
    }
    
     public void atualizar(Pessoa p){
        em.merge(p);
    }
     
     public List<Pessoa> listar(){
        List<Pessoa> lista = em.createQuery("SELECT p FROM Pessoa p").getResultList();
        return lista;           
        }
    
}
