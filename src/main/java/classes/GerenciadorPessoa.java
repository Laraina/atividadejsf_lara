package classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named(value = "gerenciadorPessoa")
@SessionScoped
public class GerenciadorPessoa implements Serializable {

    @EJB
    private Cadastro cadastro;
    private Pessoa pessoa;
    private List<Pessoa> pessoas;

    public GerenciadorPessoa() {
        cadastro = new Cadastro();
        pessoa = new Pessoa();
        pessoas = new ArrayList<>();
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Cadastro getCadastro() {
        return cadastro;
    }

    public void setCadastro(Cadastro cadastro) {
        this.cadastro = cadastro;
    }

    public List<Pessoa> getPessoas() {
        return cadastro.listar();
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public String salvar() {
        cadastro.salvar(pessoa);
        pessoa = new Pessoa();
        return "index";
    }

    public String deletar(Pessoa p) {
        cadastro.deletar(p);
        return "cadastradas";
    }

    public String atualizaDados(Pessoa p) {
        pessoa.setNome(p.getNome());
        pessoa.setCpf(p.getCpf());
        return "atualizar";
    }

    public String atualizar() {
        cadastro.atualizar(pessoa);
        pessoa = new Pessoa();
        return "cadastradas";
    }
}
